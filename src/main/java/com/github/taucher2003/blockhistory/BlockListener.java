package com.github.taucher2003.blockhistory;

import com.github.taucher2003.blockhistory.threshold.ThresholdHandler;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.time.Instant;
import java.util.logging.Level;

public class BlockListener implements Listener {

    private final DataSource dataSource;
    private final ThresholdHandler thresholdHandler;
    private final BukkitScheduler scheduler;
    private final JavaPlugin plugin;

    public BlockListener(DataSource dataSource, ThresholdHandler thresholdHandler,
                         BukkitScheduler scheduler, JavaPlugin plugin) {
        this.dataSource = dataSource;
        this.thresholdHandler = thresholdHandler;
        this.scheduler = scheduler;
        this.plugin = plugin;
    }

    private void append(BlockEvent blockEvent) {
        scheduler.runTaskAsynchronously(plugin, () -> {
            try (var connection = dataSource.getConnection();
                 var statement = connection.prepareStatement("INSERT INTO block_history (`date`, world, x, y, z, player, material, `action`) VALUES (?, ?, ?, ?, ?, ?, ?, ?)")) {
                blockEvent.fillStatement(statement);
                statement.executeUpdate();
            } catch (SQLException e) {
                Bukkit.getLogger().log(Level.SEVERE, "Failed to save history entry", e);
            }
        });
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        var block = event.getBlock();
        var location = block.getLocation();
        var blockEvent = new BlockEvent(
                Instant.now(),
                location,
                event.getPlayer(),
                block.getType(),
                BlockEvent.Action.BLOCK_PLACED
        );

        append(blockEvent);
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        var block = event.getBlock();
        var location = block.getLocation();
        var blockEvent = new BlockEvent(
                Instant.now(),
                location,
                event.getPlayer(),
                block.getType(),
                BlockEvent.Action.BLOCK_DESTROYED
        );

        append(blockEvent);

        thresholdHandler.recordBlockBreak(blockEvent);
    }
}
