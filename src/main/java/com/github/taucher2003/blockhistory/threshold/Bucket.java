/*
 * Copyright (c) 2020 - 2021, Niklas van Schrick
 *
 * Bucket.java is part of the BL4CKLIST-bot which is licensed under BSD 3-Clause "New" or "Revised" License
 */

package com.github.taucher2003.blockhistory.threshold;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class Bucket {
    private final BucketData data;

    private final AtomicInteger remaining;
    private final AtomicLong resetAt = new AtomicLong(System.currentTimeMillis());

    public Bucket(BucketData data) {
        this.data = data;
        this.remaining = new AtomicInteger(data.limit());
    }

    public boolean isRatelimit() {
        checkReset();
        return remaining.get() <= 0;
    }

    public boolean use() {
        checkReset();
        return remaining.decrementAndGet() >= 0;
    }

    public long getUsableIn(TimeUnit timeUnit) {
        return timeUnit.convert(resetAt.get() - System.currentTimeMillis(), TimeUnit.MILLISECONDS);
    }

    public BucketData getData() {
        return data;
    }

    private void checkReset() {
        var now = System.currentTimeMillis();
        if (now >= resetAt.get()) {
            remaining.set(data.limit());
            resetAt.set(now + data.resetAfterUnit().toMillis(data.resetAfter()));
        }
    }

    public static class ReadonlyBucket extends Bucket {
        private final Bucket bucket;

        public ReadonlyBucket(Bucket bucket) {
            super(bucket.data);
            this.bucket = bucket;
        }

        @Override
        public boolean isRatelimit() {
            return bucket.isRatelimit();
        }

        @Override
        public boolean use() {
            throw new UnsupportedOperationException("use is not supported on a read only bucket");
        }

        @Override
        public long getUsableIn(TimeUnit timeUnit) {
            return bucket.getUsableIn(timeUnit);
        }

        @Override
        public BucketData getData() {
            return bucket.getData();
        }
    }
}