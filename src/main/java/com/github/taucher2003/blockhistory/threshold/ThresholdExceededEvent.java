package com.github.taucher2003.blockhistory.threshold;

import com.github.taucher2003.blockhistory.BlockEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

public class ThresholdExceededEvent extends PlayerEvent {
    private static final HandlerList HANDLER_LIST = new HandlerList();

    private final Bucket bucket;
    private final BlockEvent blockEvent;

    public ThresholdExceededEvent(Player who, Bucket bucket, BlockEvent blockEvent) {
        super(who);
        this.bucket = bucket;
        this.blockEvent = blockEvent;
    }

    public Bucket getBucket() {
        return bucket;
    }

    public BlockEvent getBlockEvent() {
        return blockEvent;
    }

    @Override
    public HandlerList getHandlers() {
        return HANDLER_LIST;
    }

    public static HandlerList getHandlerList() {
        return HANDLER_LIST;
    }
}
