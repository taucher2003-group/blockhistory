package com.github.taucher2003.blockhistory.threshold;

import org.bukkit.Material;

import java.util.Objects;
import java.util.UUID;

public record PlayerThresholdKey(UUID uuid, Material material) {

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        var other = (PlayerThresholdKey) o;
        return Objects.equals(uuid, other.uuid) && material == other.material;
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid, material);
    }

    @Override
    public String toString() {
        return "PlayerThresholdKey{" +
                "uuid=" + uuid +
                ", material=" + material +
                '}';
    }
}
