package com.github.taucher2003.blockhistory.threshold;

import org.bukkit.Material;

import java.util.List;
import java.util.Objects;

public record MaterialThresholdKey(List<Material> materials, BucketData bucketData) {

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        var other = (MaterialThresholdKey) o;
        return Objects.equals(materials, other.materials) && Objects.equals(bucketData, other.bucketData);
    }

    @Override
    public int hashCode() {
        return Objects.hash(materials, bucketData);
    }

    @Override
    public String toString() {
        return "MaterialThresholdKey{" +
                "materials=" + materials +
                ", bucketData=" + bucketData +
                '}';
    }
}
