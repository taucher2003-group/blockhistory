/*
 * Copyright (c) 2020 - 2021, Niklas van Schrick
 *
 * BucketData.java is part of the BL4CKLIST-bot which is licensed under BSD 3-Clause "New" or "Revised" License
 */

package com.github.taucher2003.blockhistory.threshold;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

public record BucketData(int limit, int resetAfter, TimeUnit resetAfterUnit) {

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        var data = (BucketData) o;
        return limit == data.limit && resetAfter == data.resetAfter && resetAfterUnit == data.resetAfterUnit;
    }

    @Override
    public int hashCode() {
        return Objects.hash(limit, resetAfter, resetAfterUnit);
    }

    @Override
    public String toString() {
        return "BucketData{" +
                "limit=" + limit +
                ", resetAfter=" + resetAfter +
                ", resetAfterUnit=" + resetAfterUnit +
                '}';
    }
}
