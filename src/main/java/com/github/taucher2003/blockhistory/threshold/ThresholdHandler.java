package com.github.taucher2003.blockhistory.threshold;

import com.github.taucher2003.blockhistory.BlockEvent;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

public class ThresholdHandler {

    protected final Map<PlayerThresholdKey, Bucket> thresholdBuckets = new HashMap<>();
    protected final Map<MaterialThresholdKey, BucketData> bucketDataMap = new HashMap<>();

    public void recordBlockBreak(BlockEvent event) {
        var key = buildThresholdKey(event);
        var material = event.material();
        var materialKey = findMaterialThresholdKey(material);
        if (materialKey.isEmpty()) {
            return;
        }
        var bucket = thresholdBuckets.computeIfAbsent(key, k -> new Bucket(bucketDataMap.get(materialKey.get())));
        if (!bucket.use()) {
            Bukkit.getPluginManager().callEvent(new ThresholdExceededEvent(event.player(), new Bucket.ReadonlyBucket(bucket), event));
            thresholdBuckets.remove(key);
        }
    }

    protected PlayerThresholdKey buildThresholdKey(BlockEvent event) {
        return new PlayerThresholdKey(event.player().getUniqueId(), event.material());
    }

    protected Optional<MaterialThresholdKey> findMaterialThresholdKey(Material material) {
        return bucketDataMap.keySet().stream().filter(key -> key.materials().contains(material)).findFirst();
    }

    public void load() throws IOException {
        bucketDataMap.clear();
        var thresholdJson = loadThresholds();

        for (var obj : thresholdJson) {
            var jsonObj = (JSONObject) obj;
            var amount = jsonObj.getInt("amount");
            var duration = jsonObj.getInt("duration");
            var durationUnit = jsonObj.getEnum(TimeUnit.class, "duration_unit");
            var materialsArray = jsonObj.getJSONArray("matched_materials");
            var materials = new ArrayList<Material>();
            for (var i = 0; i < materialsArray.length(); i++) {
                materials.add(materialsArray.getEnum(Material.class, i));
            }

            var threshold = new BucketData(amount, duration, durationUnit);
            var materialKey = new MaterialThresholdKey(materials, threshold);
            bucketDataMap.put(materialKey, threshold);
        }
    }

    private JSONArray loadThresholds() throws IOException {
        var fileContent = Files.readString(Path.of("plugins", "BlockHistory", "thresholds.json"));
        return new JSONArray(fileContent);
    }
}
