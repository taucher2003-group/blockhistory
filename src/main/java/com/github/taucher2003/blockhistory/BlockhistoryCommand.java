package com.github.taucher2003.blockhistory;

import com.github.taucher2003.blockhistory.threshold.ThresholdHandler;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

public class BlockhistoryCommand implements TabExecutor {

    private final ThresholdHandler thresholdHandler;

    public BlockhistoryCommand(ThresholdHandler thresholdHandler) {
        this.thresholdHandler = thresholdHandler;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(args.length == 1 && "reload".equalsIgnoreCase(args[0]) && sender.hasPermission("blockhistory.reload")) {
            try {
                thresholdHandler.load();
            } catch (IOException e) {
                Bukkit.getLogger().log(Level.SEVERE, "Failed to reload thresholds");
            }
        }
        return false;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        var list = new ArrayList<String>();
        if(sender.hasPermission("blockhistory.reload")) {
            list.add("reload");
        }
        return list;
    }
}
