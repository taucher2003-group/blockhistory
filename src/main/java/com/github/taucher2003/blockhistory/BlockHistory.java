package com.github.taucher2003.blockhistory;

import com.github.taucher2003.blockhistory.threshold.ThresholdHandler;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

public class BlockHistory extends JavaPlugin {

    private DataSource dataSource;

    @Override
    public void onEnable() {
        saveDefaultConfig();
        var pluginConfig = getConfig();

        var config = new HikariConfig();
        config.setJdbcUrl("jdbc:mysql://" + pluginConfig.getString("db.host") + ":" + pluginConfig.getInt("db.port") + "/" + pluginConfig.getString("db.db"));
        config.setUsername(pluginConfig.getString("db.user"));
        config.setPassword(pluginConfig.getString("db.password"));

        config.setMaximumPoolSize(5);
        config.setMaxLifetime(TimeUnit.MINUTES.toMillis(9));

        dataSource = new HikariDataSource(config);
        createTable();

        var thresholdHandler = new ThresholdHandler();
        Bukkit.getPluginManager().registerEvents(new BlockListener(dataSource, thresholdHandler, Bukkit.getScheduler(), this), this);
        var command = new BlockhistoryCommand(thresholdHandler);
        var bukkitCommand = getCommand("blockhistory");
        if(bukkitCommand != null) {
            bukkitCommand.setExecutor(command);
            bukkitCommand.setTabCompleter(command);
        }

        try {
            thresholdHandler.load();
        } catch (IOException e) {
            getLogger().log(Level.SEVERE, "Failed to read threshold config");
        }
    }

    private void createTable() {
        try (var connection = dataSource.getConnection(); var statement = connection.createStatement()) {
            statement.executeUpdate("""
                CREATE TABLE IF NOT EXISTS block_history (
                    id BIGINT AUTO_INCREMENT,
                    `date` DATETIME,
                    world VARCHAR(30),
                    x INT,
                    y INT,
                    z INT,
                    player BINARY(16),
                    material VARCHAR(50),
                    `action` VARCHAR(50),
                    PRIMARY KEY (id)
                )
                """);
        } catch (SQLException exception) {
            getLogger().log(Level.SEVERE, "Failed to create table", exception);
        }
    }

    @Override
    public void onDisable() {
        ((HikariDataSource) dataSource).close();
    }
}
