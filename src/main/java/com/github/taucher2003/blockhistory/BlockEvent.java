package com.github.taucher2003.blockhistory;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;

public record BlockEvent(Instant instant, Location location, Player player, Material material, Action action) {

    public void fillStatement(PreparedStatement statement) throws SQLException {
        statement.setTimestamp(1, new Timestamp(instant.toEpochMilli()));
        statement.setString(2, location.getWorld().getName());
        statement.setInt(3, location.getBlockX());
        statement.setInt(4, location.getBlockY());
        statement.setInt(5, location.getBlockZ());
        statement.setBytes(6, Helpers.uuidToByte(player.getUniqueId()));
        statement.setString(7, material.name());
        statement.setString(8, action.name());
    }

    public enum Action {
        BLOCK_PLACED,
        BLOCK_DESTROYED
    }
}
