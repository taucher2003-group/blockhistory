package com.github.taucher2003.blockhistory;

import java.nio.ByteBuffer;
import java.util.UUID;

public final class Helpers {
    private Helpers() {
    }

    public static byte[] uuidToByte(UUID uuid) {
        return ByteBuffer.wrap(new byte[16])
                .putLong(uuid.getMostSignificantBits())
                .putLong(uuid.getLeastSignificantBits())
                .array();
    }

    public static UUID byteToUUID(byte[] bytes) {
        var byteBuffer = ByteBuffer.wrap(bytes);
        return new UUID(byteBuffer.getLong(), byteBuffer.getLong());
    }
}
